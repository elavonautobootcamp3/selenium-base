package com.elavon.test;

import static com.elavon.util.Utility.pause;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.elavon.util.Utility;

public class DemoQAExercise {

	static WebDriver driver;

	@BeforeClass
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
	}

	@Before
	public void initDriver() {
		driver = new ChromeDriver();
	}

	@After
	public void closeDriver() {
		driver.close();
		driver.quit();
	}

	/**
	 * Do the exercise on this method
	 */

	@Test
	public void testSelect() {
		// TODO: Automate the script below using Selenium, JUnit and AssertJ
		// Use DemoQAStory and SeleniumStory1 classes as your reference
		// Reuse the Utility class methods and add methods for your convenience
		
		/*
		Test Script:
			1. Open the browser to [http://demoqa.com/registration/]
			2. Enter your First Name and Last Name on the Registration Form
			3. Select Single as Marital Status
			4. Select all hobbies
			5. Select Philippines as Country
			6. Enter you Date of Birth
			7. Enter 1234567890 on the Phone Number field
			8. Enter 'username' on the Username field
			8. Enter user@email.com on the E-mail field
			9. Do nothing on the Profile Picture and About Yourself fields
			10. Enter 'password' as Password 
			11. Enter 'wordpass' as Confirm Password
			12. Verify if the password strength indicator reads as 'Mismatch'
			13. Enter 'password' as Confirm Password
			14. Verify if the password strength indicator reads as 'Very weak'
			15. Enter 'Th1sIsAStrongP@ssw0rkZ' as Password and Confirm Password
			16. Verify if the password strength indicator reads as 'Strong'
			17. Submit the form by clicking [Submit]
			18. [Optional Step] Verify if an error message appears and reads as 'Username already exists'
		*/
	}

}
