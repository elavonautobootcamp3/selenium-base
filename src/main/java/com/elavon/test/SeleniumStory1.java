package com.elavon.test;

import static org.assertj.core.api.Assertions.assertThat;
import junit.framework.TestCase;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumStory1 extends TestCase{
	
	@Test
	public void test1() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://mvnrepository.com");
		
		driver.findElement(By.linkText("Categories")).click();
		
		WebElement heading = driver.findElement(By.xpath("//*[@id='maincontent']/h1"));
		String actual = heading.getText();
		
		String expected = "Top categorieX";
		
		//AssertJ
		assertThat(expected).isEqualToIgnoringCase(actual);
		
		driver.quit();
	}

}
