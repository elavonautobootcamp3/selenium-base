package com.elavon.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class Story0 {
	
	@BeforeClass
	public static void setup() {
		System.out.println("Setup complete");
	}
	
	@AfterClass
	public static void finalization() {
		System.out.println("Test execution complete");
	}
	
	@Test
	public void test1() {
		System.out.println(1);
	}
	
	@Test
	public void test2() {
		System.out.println(2);
	}

}
