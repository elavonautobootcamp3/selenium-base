package com.elavon.test;

import static com.elavon.util.Utility.pause;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.elavon.util.Utility;

public class DemoQAStory {
	
	static WebDriver driver;
	
	@BeforeClass
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
	}
	
	@Before
	public void initDriver() {
		driver = new ChromeDriver();
	}
	
	@After
	public void closeDriver() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void testSelect() {
		driver.get("http://demoqa.com/registration/");
		System.out.println(driver.getTitle());
		
		Select countrySelect = new Select(driver.findElement(By.id("dropdown_7")));
		countrySelect.selectByVisibleText("Mexico");
		
		List<String> countryOptions = Utility.getStringOptions(countrySelect.getOptions());
		
		//AssertJ
		assertThat(countryOptions).contains("FranceX");
		
		assertThat(countrySelect.getFirstSelectedOption().getText()).isEqualTo("Mexico");
		pause(2000);
	}
	
	@Test
	public void testRadio() {
		driver.get("http://demoqa.com/registration/");
		System.out.println(driver.getTitle());
		List<WebElement> maritalStatus = driver.findElements(By.name("radio_4[]"));
		Utility.radioCheckboxSelect(maritalStatus, "married");
		pause(2000);
	}
	
	@Test
	public void testCheckbox() {
		driver.get("http://demoqa.com/registration/");
		System.out.println(driver.getTitle());
		
		List<String> hobbiesToSelect = new ArrayList<String>();
		hobbiesToSelect.add("dance");
		hobbiesToSelect.add("cricket");
		List<WebElement> hobby = driver.findElements(By.name("checkbox_5[]"));
		Utility.checkboxSelect(hobby, hobbiesToSelect);
		pause(2000);
	}

}
