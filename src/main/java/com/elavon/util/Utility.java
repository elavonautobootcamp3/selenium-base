package com.elavon.util;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class Utility {
	public static List<String> getStringOptions(List<WebElement> options) {
		List<String> strOptions = new ArrayList<String>();
		
		for(WebElement opt : options) {
			strOptions.add(opt.getText());
		}
		
		return strOptions;
	}
	
	public static void radioCheckboxSelect(List<WebElement> options, String value) {
		for(WebElement opt : options) {
			if(value.equals(opt.getAttribute("value").trim())) {
				opt.click();
			}
		}
	}
	
	public static void checkboxSelect(List<WebElement> options, List<String> value) {
		for(WebElement opt : options) {
			if(value.contains(opt.getAttribute("value").trim())) {
				opt.click();
			}
		}
	}
	
	public static void pause(long milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
